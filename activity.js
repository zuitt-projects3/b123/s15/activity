function oddEvenChecker(number){
  if(typeof number === "number"){
    console.log("This is a number")

    if(number%2 == 0){
      console.log("The number is even!")
    }else{
      console.log("The number is odd!")
    }

  }else{
    alert("Invalid Input!")
  }
}

function budgetChecker(number2){
  if(typeof number2 === "number"){
    console.log("This is a number")

    if(number2>40000){
      console.log("You are over the Budget!")
    }else{
      console.log("You have Resources Left!")
    }
  }else{
    alert("Invalid Input!")
  }
}
